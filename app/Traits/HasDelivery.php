<?php
/**
 * Created by PhpStorm.
 * User: swatfizik
 * Date: 05.03.2019
 * Time: 19:09
 */

namespace App\Traits;


use App\Models\Status;
use Illuminate\Support\Facades\DB;

trait HasDelivery
{
    public function scopeUnreaded($query)
    {
        return $query
            ->where(static::TABLE . '.delivery_status_id', static::getUnreadedStatus());
    }

    public static function getDeliveryStatus(int $code)
    {
        return Status::getId($code, static::TABLE);
    }

    /**
     * @return int|null
     */
    public static function getReadedStatus()
    {
        return Status::getId(Status::CODE_READED, static::TABLE);
    }

    /**
     * @return int|null
     */
    public static function getUnreadedStatus()
    {
        return Status::getId(Status::CODE_UNREADED, static::TABLE);
    }


    /**
     * @param $ids
     */
    public static function toggleToReadedStatus($ids)
    {
        DB::table(static::TABLE)
            ->whereIn('id', $ids)
            ->update(array('delivery_status_id' => static::getReadedStatus()));
    }
}
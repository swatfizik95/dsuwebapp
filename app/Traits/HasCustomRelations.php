<?php
/**
 * Created by PhpStorm.
 * User: swatfizik
 * Date: 18.02.2019
 * Time: 18:47
 */

namespace App\Traits;

use App\Relations\CustomRelation;
use Closure;


trait HasCustomRelations
{
    /**
     * Define a custom relationship.
     *
     * @param  string  $related
     * @param  string  $baseConstraints
     * @param  string  $eagerConstraints
     * @return \App\Services\Database\Relations\Custom
     */
    public function custom($related, Closure $baseConstraints, Closure $eagerConstraints)
    {
        $instance = new $related;
        $query = $instance->newQuery();

        return new CustomRelation($query, $this, $baseConstraints, $eagerConstraints);
    }
}
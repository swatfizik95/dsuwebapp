<?php
/**
 * Created by PhpStorm.
 * User: swatfizik
 * Date: 06.02.2019
 * Time: 16:20
 */

namespace App\Traits;


use App\Models\Status;

trait Activity
{
    public function scopeActive($query)
    {
        return $query->where(static::TABLE . '.status_id', static::getActiveStatus());
    }

    /**
     * @return int|null
     */
    public final static function getActiveStatus()
    {
        return Status::getId(Status::CODE_ACTIVE, static::TABLE);
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const TABLE = 'statuses';

    // Стандартные коды статусов
    const CODE_ACTIVE = 1;
    const CODE_NOACTIVE = 2;

    const CODE_READED = 20;
    const CODE_UNREADED = 21;

    const CODE_FINISHED = 30;
    const CODE_STARTED = 31;

    const CODE_UPDATED = 40;

    public $timestamps = false;

    protected $table = self::TABLE;

    /**
     * @param int $code
     * @param string $tableName
     * @return int|null
     */
    public static function getId(int $code, string $tableName){
        return Status::where([['code', $code], ['table', $tableName]])->value('id') ?? null;
    }
}

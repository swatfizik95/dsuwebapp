<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model
{
    use Activity;

    const TABLE = 'student_infos';

    protected $table = self::TABLE;
}

<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class CourseVote extends Model
{
    use Activity;

    const TABLE = 'course_votes';
    protected $table = self::TABLE;
}

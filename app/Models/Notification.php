<?php

namespace App\Models;

use App\Traits\Activity;
use App\Traits\HasCustomRelations;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use Symfony\Component\Console\Helper\Table;

/**
 * Class Notification
 * @package App\Models
 */
class Notification extends Model
{
    use Activity, HasRelationships;

    const TABLE = 'notifications';
    protected $table = self::TABLE;

    #region Relations

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveries()
    {
        return $this
            ->hasMany(NotificationDelivery::class, 'notification_id')
            ->where(NotificationDelivery::TABLE . '.status_id', NotificationDelivery::getActiveStatus());
    }

    public function delivery()
    {
        return $this
            ->hasOne(NotificationDelivery::class, 'notification_id')
            ->where(NotificationDelivery::TABLE . '.status_id', NotificationDelivery::getActiveStatus());
    }

    public function user()
    {
        return $this
            ->hasOne(User::class, 'user_id')
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }

    /**
     * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
     */
    public function usersByRoles()
    {
        return $this->hasManyDeep(
            User::class,
            [RoleNotification::class, UserRole::class],
            ['notification_id', 'role_id', 'id'],
            ['id', 'role_id', 'user_id']);
    }


    public function usersByGroup()
    {
        return $this->hasManyDeep(
            User::class,
            [GroupNotification::class, StudentInfo::class],
            ['notification_id', 'group_id', 'id'],
            ['id', 'group_id', 'user_id']);
    }

    /**
     * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
     */
    public function usersByCourse()
    {
        return $this->hasManyDeep(
            User::class,
            [CourseNotification::class, StudentInfo::class],
            ['notification_id', 'course_id', 'id'],
            ['id', 'course_id', 'user_id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function createdBy()
    {
        return $this
            ->hasOne(User::class, 'id', 'created_by')
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this
            ->belongsToMany(Role::class, RoleNotification::TABLE, 'notification_id', 'role_id')
            ->where(RoleNotification::TABLE . '.status_id', RoleNotification::getActiveStatus());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this
            ->belongsToMany(Group::class, GroupNotification::TABLE, 'notification_id', 'group_id')
            ->where(GroupNotification::TABLE . '.status_id', GroupNotification::getActiveStatus())
            ->where(Group::TABLE . '.status_id', Group::getActiveStatus());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this
            ->belongsToMany(Course::class, CourseNotification::TABLE, 'notification_id', 'course_id')
            ->where(CourseNotification::TABLE . '.status_id', CourseNotification::getActiveStatus())
            ->where(Course::TABLE . '.status_id', Course::getActiveStatus());
    }

    #endregion

    #region Scope

    public function scopeForUser($query, $userId)
    {
        return $query->whereHas('deliveries', function ($query) use ($userId) {
            $query->where(NotificationDelivery::TABLE . '.user_id', $userId);
        });
    }

    public function scopeForUserWithDeliveryStatus($query, $userId, $code)
    {
        return $query->whereHas('deliveries', function ($query) use ($userId, $code) {
            $query->where([
                [NotificationDelivery::TABLE . '.user_id', $userId],
                [NotificationDelivery::TABLE . '.delivery_status_id', NotificationDelivery::getDeliveryStatus($code)]
            ]);
        });
    }

    public function scopeWithCreatedBy($query)
    {
        return $query->with(['createdBy:id,login,name']);
    }

    public function scopeWithDelivery($query, $userId)
    {
        return $query->with(
            [
                'delivery' => function ($query) use ($userId){
                return $query->where('user_id', '=', $userId)->select('id', 'notification_id', 'delivery_status_id');}
            ]);
    }

    #endregion

    #region Methods

    /**
     * @param int $createdBy
     * @param $message
     * @param $groups
     * @param $courses
     * @return Notification
     */
    public static function add(int $createdBy, $message, $groups, $courses, $roles)
    {
        $notification = new static;
        $notification->created_by = $createdBy;
        $notification->message = $message;
        $notification->status_id = Notification::getActiveStatus();
        $notification->save();
        $notification->groups()->attach($groups, ['status_id' => GroupNotification::getActiveStatus()]);
        $notification->courses()->attach($courses, ['status_id' => CourseNotification::getActiveStatus()]);
        $notification->roles()->attach($roles, ['status_id' => RoleNotification::getActiveStatus()]);

        $onlyHeadman = in_array(Role::getId(Role::HEADMAN), $roles);

        $userIds = $notification->getUserIds($onlyHeadman);
        $userIds->push($createdBy);

        $deliveries = [];
        $statusId = NotificationDelivery::getActiveStatus();
        $deliveryStatusId = NotificationDelivery::getUnreadedStatus();

        foreach ($userIds as $userId)
        {
            $delivery = new NotificationDelivery();
            $delivery->user_id = $userId;
            $delivery->notification_id = $notification->id;
            $delivery->status_id = $statusId;
            $delivery->delivery_status_id = $deliveryStatusId;
            $deliveries[] = $delivery;
        }

        $notification->deliveries()->saveMany($deliveries);

        return $notification;
    }

    public static function getAll($userId)
    {
        return Notification::active()
            ->forUser($userId)
            ->withCreatedBy()
            ->withDelivery($userId)
            ->orderBy('created_at', 'desc')
            ->get(['id', 'message', 'created_at', 'created_by']);
    }

    public static function getAllRead($userId)
    {
        return Notification::active()
            ->forUserWithDeliveryStatus($userId, Status::CODE_READED)
            ->withCreatedBy()
            ->withDelivery($userId)
            ->orderBy('created_at', 'desc')
            ->get(['id', 'message', 'created_at', 'created_by']);
    }

    public static function getAllUnread($userId)
    {
        $notifications = Notification::active()
            ->forUserWithDeliveryStatus($userId, Status::CODE_UNREADED)
            ->withCreatedBy()
            ->withDelivery($userId)
            ->orderBy('created_at', 'desc')
            ->get(['id', 'message', 'created_at', 'created_by']);

        NotificationDelivery::toggleToReadedStatus($notifications->pluck('delivery.id'));

        return $notifications;
    }

    public static function get($userId, $id)
    {
        $id =  NotificationDelivery::getNotificationId($userId, $id);

        return Notification::active()
            ->where('id', $id)
            ->first(['id', 'message', 'created_at']);
    }

    public function getUserIds($onlyHeadman)
    {
        $usersByGroup = $this->usersByGroup()->active();
        $usersByCourse = $this->usersByCourse()->active();
        $usersByRole = $this->usersByRoles()->active();

        if ($onlyHeadman)
        {
            $usersByGroup->headman();
            $usersByCourse->headman();
            $usersByRole->headman();
        }

        $usersByGroup = $usersByGroup->get([User::TABLE . '.id']);
        $usersByCourse = $usersByCourse->get([User::TABLE . '.id']);
        $usersByRole = $usersByRole->get([User::TABLE . '.id']);

        return $usersByGroup->intersect($usersByCourse->intersect($usersByRole))->pluck('id');
    }

    #endregion
}

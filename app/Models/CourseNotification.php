<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class CourseNotification extends Model
{
    use Activity;

    const TABLE = 'course_notifications';

    protected $table = self::TABLE;
}

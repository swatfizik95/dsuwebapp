<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use Activity;

    const TABLE = 'groups';

    protected $table = self::TABLE;

    protected $hidden = [
        'status_id'
    ];

    #region Relations

    public function users()
    {
        return $this
            ->belongsToMany(User::class, StudentInfo::TABLE, 'group_id', 'user_id')
            ->where(StudentInfo::TABLE . '.status_id', StudentInfo::getActiveStatus());
    }

    #endregion

    #region Methods

    /**
     * @return Group[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return Group::active()->get();
    }

    public static function search($query)
    {
        return Group::active()->where('name', 'LIKE', '%'.$query.'%')->get();
    }

    #endregion
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const TABLE = 'roles';

    const ADMIN = 1;
    const SECRETARIAT = 2;
    const TEACHER = 3;
    const STUDENT = 4;

    const BACHELOR = 10;
    const MASTER = 11;

    const HEADMAN = 21;

    public $timestamps = false;
    protected $table = self::TABLE;

    //region relations

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this
            ->belongsToMany(User::class, UserRole::TABLE, 'role_id', 'user_id')
            ->where(UserRole::TABLE . '.status_id', UserRole::getActiveStatus())
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }

    //endregion

    /**
     * @param int $code
     * @return int
     */
    public static function getId(int $code){
        return Role::where('code', $code)->value('id') ?? null;
    }

    public static function getRoleOfStudents()
    {
        return Role::whereIn('code', [self::BACHELOR, self::MASTER, self::HEADMAN])->get(['id', 'description']);
    }
}

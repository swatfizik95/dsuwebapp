<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use Activity;

    const TABLE = 'user_roles';

    protected $fillable = [
        'role_id',
        'user_id',
        'status_id'
    ];

//    protected $hidden = [
//        'status_id'
//    ];

    protected $table = self::TABLE;
    public $timestamps = false;
}

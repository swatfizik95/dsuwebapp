<?php

namespace App\Models;

use App\Traits\Activity;
use App\Traits\HasDelivery;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VoteDelivery extends Model
{
    use Activity, HasDelivery;

    const TABLE = 'vote_deliveries';
    protected $table = self::TABLE;

    #region Relations

    public function user()
    {
        return $this
            ->hasOne(User::class, 'id', 'user_id')
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }

    public function vote()
    {
        return $this
            ->hasOne(Vote::class, 'id', 'vote_id')
            ->where(Vote::TABLE . '.status_id', Vote::getActiveStatus());
    }

    #endregion

    #region Methods

    /**
     * @return int|null
     */
    public static function getUpdatedStatus()
    {
        return Status::getId(Status::CODE_UPDATED, self::TABLE);
    }

    #endregion
}
<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class GroupNotification extends Model
{
    use Activity;

    const TABLE = 'group_notifications';

    protected $table = self::TABLE;
}

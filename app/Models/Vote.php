<?php

namespace App\Models;

use App\ResponseHelper;
use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Vote extends Model
{
    use Activity, HasRelationships;

    const TABLE = 'votes';
    protected $table = self::TABLE;

    #region Relations
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveries()
    {
        return $this
            ->hasMany(VoteDelivery::class, 'vote_id')
            ->where(VoteDelivery::TABLE . '.status_id', VoteDelivery::getActiveStatus());
    }

    public function delivery()
    {
        return $this
            ->hasOne(VoteDelivery::class, 'vote_id')
            ->where(VoteDelivery::TABLE . '.status_id', VoteDelivery::getActiveStatus());
    }

    public function user()
    {
        return $this
            ->hasOne(User::class, 'user_id')
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }

    /**
     * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
     */
    public function usersByRoles()
    {
        return $this->hasManyDeep(
            User::class,
            [RoleVote::class, UserRole::class],
            ['vote_id', 'role_id', 'id'],
            ['id', 'role_id', 'user_id']);
    }

    public function usersByGroup()
    {
        return $this->hasManyDeep(
            User::class,
            [GroupVote::class, StudentInfo::class],
            ['vote_id', 'group_id', 'id'],
            ['id', 'group_id', 'user_id']);
    }

    /**
     * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
     */
    public function usersByCourse()
    {
        return $this->hasManyDeep(
            User::class,
            [CourseVote::class, StudentInfo::class],
            ['vote_id', 'course_id', 'id'],
            ['id', 'course_id', 'user_id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function createdBy()
    {
        return $this
            ->hasOne(User::class, 'id', 'created_by')
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this
            ->belongsToMany(Role::class, RoleVote::TABLE, 'vote_id', 'role_id')
            ->where(RoleVote::TABLE . '.status_id', RoleVote::getActiveStatus());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this
            ->belongsToMany(Group::class, GroupVote::TABLE, 'vote_id', 'group_id')
            ->where(GroupVote::TABLE . '.status_id', GroupVote::getActiveStatus())
            ->where(Group::TABLE . '.status_id', Group::getActiveStatus());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this
            ->belongsToMany(Course::class, CourseVote::TABLE, 'vote_id', 'course_id')
            ->where(CourseVote::TABLE . '.status_id', CourseVote::getActiveStatus())
            ->where(Course::TABLE . '.status_id', Course::getActiveStatus());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this
            ->hasMany(VoteAnswer::class, 'vote_id')
            ->where(VoteAnswer::TABLE . '.status_id', VoteAnswer::getActiveStatus());
    }

    public function answer()
    {
        return $this
            ->hasOne(VoteAnswer::class, 'id', 'answer_id')
            ->where(VoteAnswer::TABLE . '.status_id', VoteAnswer::getActiveStatus());
    }

    public function userAnswers()
    {
        return $this
            ->hasMany(VoteDelivery::class, 'vote_id')
            ->where(VoteDelivery::TABLE . '.status_id', VoteDelivery::getActiveStatus())
            ->whereNotNull('answer_id')
            ->groupby('answer_id')
            ->select('answer_id', 'vote_id', DB::raw('count(*) as count'));
    }
    
    #endregion
    
    #region Scope

    public function scopeForUser($query, $userId)
    {
        return $query->whereHas('deliveries', function ($query) use ($userId) {
            $query->where(VoteDelivery::TABLE . '.user_id', $userId);
        });
    }

    public function scopeForUserWithDeliveryStatus($query, $userId, $code)
    {
        return $query->whereHas('deliveries', function ($query) use ($userId, $code) {
            $query->where([
                [VoteDelivery::TABLE . '.user_id', $userId],
                [VoteDelivery::TABLE . '.delivery_status_id', VoteDelivery::getDeliveryStatus($code)]
            ]);
        });
    }

    public function scopeForUserWithNotDeliveryStatus($query, $userId, $code)
    {
        return $query->whereHas('deliveries', function ($query) use ($userId, $code) {
            $query->where([
                [VoteDelivery::TABLE . '.user_id', $userId],
                [VoteDelivery::TABLE . '.delivery_status_id', '!=', VoteDelivery::getDeliveryStatus($code)]
            ]);
        });
    }

    public function scopeWithCreatedBy($query)
    {
        return $query->with(['createdBy:id,login,name']);
    }

    public function scopeWithAnswers($query)
    {
        return $query->with(['answers:id,text,vote_id']);
    }

    public function scopeWithAnswer($query)
    {
        return $query->with(['answer:id,text,vote_id']);
    }

    public function scopeWithUserAnswers($query)
    {
        return $query->with(['userAnswers']);
    }

    public function scopeWithDelivery($query, $userId)
    {
        return $query->with(
            [
                'delivery' => function ($query) use ($userId){
                    return $query->where('user_id', '=', $userId)->select('id', 'vote_id', 'user_id', 'delivery_status_id', 'answer_id');}
            ]);
    }
    
    #endregion

    #region Methods

    public static function add(int $createdBy, $text, $groups, $courses, $roles, $answers)
    {
        $vote = new static;
        $vote->created_by = $createdBy;
        $vote->text = $text;
        $vote->status_id = Vote::getActiveStatus();
        $vote->vote_status_id = Vote::getStartedStatus();
        $vote->save();
        $vote->groups()->attach($groups, ['status_id' => GroupVote::getActiveStatus()]);
        $vote->courses()->attach($courses, ['status_id' => CourseVote::getActiveStatus()]);
        $vote->roles()->attach($roles, ['status_id' => RoleVote::getActiveStatus()]);

        $statusId = VoteAnswer::getActiveStatus();

        $voteAnswers = [];

        foreach ($answers as $answer)
        {
            $voteAnswer = new VoteAnswer();
            $voteAnswer->text = $answer['text'];
            $voteAnswer->status_id = $statusId;
            $voteAnswer->vote_id = $vote->id;
            $voteAnswers[] = $voteAnswer;
        }
        $vote->answers()->saveMany($voteAnswers);

        $onlyHeadman = in_array(Role::getId(Role::HEADMAN), $roles);
        $userIds = $vote->getUserIds($onlyHeadman);
        $userIds->push($createdBy);
        $deliveries = [];
        $statusId = VoteDelivery::getActiveStatus();
        $deliveryStatusId = VoteDelivery::getUnreadedStatus();

        foreach ($userIds as $userId)
        {
            $delivery = new VoteDelivery();
            $delivery->user_id = $userId;
            $delivery->vote_id = $vote->id;
            $delivery->status_id = $statusId;
            $delivery->delivery_status_id = $deliveryStatusId;
            $deliveries[] = $delivery;
        }

        $vote->deliveries()->saveMany($deliveries);

        return $vote;
    }

    public static function finish($id, $userId)
    {
        $vote = Vote::active()
            ->where([['id', $id], ['created_by', $userId]])
            ->first(['id', 'vote_status_id']);

        if ($vote == null)
            return "vote was null";
        if ($vote->vote_status_id == Vote::getFinishedStatus())
            return "vote was finished";

        $vote->vote_status_id = Vote::getFinishedStatus();

        if ($vote
            ->deliveries()
            ->whereNotNull('answer_id')
            ->first(['answer_id']) == null)
        {
            $vote->answer_id = $vote->answers()->value('id');
            $vote->answers()->value('id');
        }
        else
        {
            $vote->answer_id = $vote->userAnswers()
                ->orderBy('count', 'desc')
                ->orderBy('answer_id', 'asc')
                ->first()->answer_id;

            $vote->userAnswers()->get();
        }

        $vote->save();
        $vote->deliveries()
            ->where('delivery_status_id', '!=', VoteDelivery::getUnreadedStatus())
            ->update(['delivery_status_id' => VoteDelivery::getUpdatedStatus()]);

        return ResponseHelper::OK;
    }

    /**
     * @param $query
     * @param $userId
     * @return
     */
    private static function queryGet($query, $userId)
    {
        return $query
            ->withCreatedBy()
            ->withAnswer()
            ->withAnswers()
            ->withUserAnswers()
            ->withDelivery($userId)
            ->orderBy('created_at', 'desc')
            ->get(['id', 'text', 'created_at', 'created_by', 'vote_status_id', 'answer_id']);
    }

    public static function getAll($userId)
    {
        return self::queryGet(Vote::active()
            ->forUser($userId), $userId);
    }

    public static function getAllRead($userId)
    {
        return self::queryGet(Vote::active()
            ->forUserWithNotDeliveryStatus($userId, Status::CODE_UNREADED), $userId);
    }

    public static function getAllUnread($userId)
    {
        $votes = self::queryGet(Vote::active()
            ->forUserWithDeliveryStatus($userId, Status::CODE_UNREADED), $userId);

        VoteDelivery::toggleToReadedStatus($votes->pluck('delivery.id'));

        return $votes;
    }

    public static function getAllUpdated($userId)
    {
        $votes = self::queryGet(Vote::active()->forUserWithDeliveryStatus($userId, Status::CODE_UPDATED), $userId);

        VoteDelivery::toggleToReadedStatus($votes->pluck('delivery.id'));

        return $votes;
    }

    public static function updateAnswer($id, $userId, $answerId)
    {
        $vote_status_id = VoteDelivery::active()
            ->where([['vote_id', $id], ['user_id', $userId]])
            ->first(['vote_id'])
            ->vote()
            ->value('vote_status_id');

        if ($vote_status_id == null)
        {
            throw new \Exception('vote was null');
        }
        if ($vote_status_id == Vote::getFinishedStatus())
        {
            throw new \Exception('vote was finished');
        }

        $answersId = VoteAnswer::active()
            ->where('vote_id', $id)
            ->get(['id']);
        if (!$answersId->contains($answerId))
        {
            throw new \Exception('answer is not exist');
        }

        VoteDelivery::active()
            ->where([['vote_id', $id], ['user_id', '!=', $userId], ['delivery_status_id', '!=', VoteDelivery::getUnreadedStatus()]])
            ->update(['delivery_status_id' => VoteDelivery::getUpdatedStatus()]);

        VoteDelivery::active()
            ->where([['vote_id', $id], ['user_id', $userId]])
            ->limit(1)
            ->update(['answer_id' => $answerId]);

        $vote = Vote::active()
            ->where('id', $id)
            ->withCreatedBy()
            ->withAnswer()
            ->withAnswers()
            ->withUserAnswers()
            ->withDelivery($userId)
            ->orderBy('created_at', 'desc')
            ->first(['id', 'text', 'created_at', 'created_by', 'vote_status_id', 'answer_id']);

        return $vote;
    }

    public function getUserIds($onlyHeadman)
    {
        $usersByGroup = $this->usersByGroup()->active();
        $usersByCourse = $this->usersByCourse()->active();
        $usersByRole = $this->usersByRoles()->active();

        if ($onlyHeadman)
        {
            $usersByGroup->headman();
            $usersByCourse->headman();
            $usersByRole->headman();
        }

        $usersByGroup = $usersByGroup->get([User::TABLE . '.id']);
        $usersByCourse = $usersByCourse->get([User::TABLE . '.id']);
        $usersByRole = $usersByRole->get([User::TABLE . '.id']);

        return $usersByGroup->intersect($usersByCourse->intersect($usersByRole))->pluck('id');
    }

    /**
     * @return int|null
     */
    public static function getStartedStatus()
    {
        return Status::getId(Status::CODE_STARTED, self::TABLE);
    }

    /**
     * @return int|null
     */
    public static function getFinishedStatus()
    {
        return Status::getId(Status::CODE_FINISHED, self::TABLE);
    }

    #endregion
}

<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use Activity;

    const TABLE = 'courses';

    protected $table = self::TABLE;

    protected $hidden = [
        'status_id'
    ];

    #region Relations
    #endregion

    #regin Methods

    /**
     * @return Course[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getALL()
    {
        return Course::all();
    }

    #endregion
}

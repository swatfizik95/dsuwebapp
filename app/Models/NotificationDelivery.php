<?php

namespace App\Models;

use App\Traits\Activity;
use App\Traits\HasDelivery;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NotificationDelivery extends Model
{
    use Activity, HasDelivery;

    const TABLE = 'notification_deliveries';

    protected $table = self::TABLE;

    //region Relations

    public function user()
    {
        return $this
            ->hasOne(User::class, 'id', 'user_id')
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }
    
    public function notification()
    {
        return $this
            ->hasOne(Notification::class, 'id', 'notification_id')
            ->where(Notification::TABLE . '.status_id', Notification::getActiveStatus());
    }

    //endregion


    //region Methods

    /**
     * @param $userId
     * @return array
     */
    public static function getNotificationIdAll($userId)
    {
        return NotificationDelivery::active()
            ->where('user_id', $userId)
            ->get(['notification_id']);
    }

    /**
     * @param $userId
     * @return array
     */
    public static function getNotificationIdAllUnreaded($userId)
    {
        $notificationIds = NotificationDelivery::active()
            ->unreaded()
            ->where('user_id', $userId)
            ->get(['notification_id']);

        return $notificationIds;
    }

    /**
     * @param $userId
     * @param $id
     * @return integer
     */
    public static function getNotificationId($userId, $id)
    {
        return NotificationDelivery::active()
            ->where('notification_id', $id)
            ->where('user_id', $userId)
            ->value('notification_id');
    }

    //endregion
}

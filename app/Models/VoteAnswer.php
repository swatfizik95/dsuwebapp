<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class VoteAnswer extends Model
{
    use Activity;

    const TABLE = 'vote_answers';
    protected $table = self::TABLE;
    public $timestamps = false;
}

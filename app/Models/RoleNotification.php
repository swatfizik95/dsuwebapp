<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class RoleNotification extends Model
{
    use Activity;

    const TABLE = 'role_notifications';

    protected $table = self::TABLE;
}

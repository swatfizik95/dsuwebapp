<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Activity;

    const TABLE = 'users';

    protected $table = self::TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'status_id'
    ];

    //region Relations

    public function roles()
    {
        return $this
            ->belongsToMany(Role::class, UserRole::TABLE, 'user_id', 'role_id')
            ->where(UserRole::TABLE . '.status_id', UserRole::getActiveStatus());
    }

    //endregion

    //region Scope

    public function scopeHeadman($query)
    {
        return $query->whereHas(Role::TABLE, function ($query) {
            $query->where(Role::TABLE . '.id', Role::getId(Role::HEADMAN));
        });
    }

    //endregion

    //region Methods

    /**
     * @param $login
     * @return User
     */
    public static function getByLogin($login)
    {
        return User::active()->where('login', $login)->first();
    }

    /**
     * @param $password
     * @return bool
     */
    public function loginAttempt($password)
    {
        return (Hash::check($password, User::where('id', $this->id)->value('password')));
    }

    //endregion
}

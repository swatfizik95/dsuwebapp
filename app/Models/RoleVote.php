<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class RoleVote extends Model
{
    use Activity;

    const TABLE = 'role_votes';
    protected $table = self::TABLE;
}

<?php

namespace App\Models;

use App\Traits\Activity;
use Illuminate\Database\Eloquent\Model;

class GroupVote extends Model
{
    use Activity;

    const TABLE = 'group_votes';
    protected $table = self::TABLE;
}

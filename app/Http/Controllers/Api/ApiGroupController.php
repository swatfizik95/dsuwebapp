<?php

namespace App\Http\Controllers\Api;

use App\Models\Group;
use App\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiGroupController extends Controller
{
    public function index()
    {
        $groups = Group::getAll();

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'groups' => $groups]);
    }

    public function search($query)
    {
        $groups = Group::search($query);

        return response()->json($groups);
    }
}

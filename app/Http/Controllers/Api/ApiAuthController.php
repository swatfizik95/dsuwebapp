<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\ResponseHelper;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ApiAuthController extends Controller
{
    public function login(Request $request)
    {
        $user = User::getByLogin($request->login);

        if ($user != null && $user->loginAttempt($request->password))
        {
            $token = $user->createToken('Personal Access Token');

            return response()->json([
                ResponseHelper::MESSAGE => ResponseHelper::OK,
                'accessToken' => $token->accessToken
            ]);
        }

        return response()->json([
            ResponseHelper::MESSAGE => ResponseHelper::ERROR,
            ResponseHelper::INNER_MESSAGE => 'Unauthorized',
        ], 401);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            ResponseHelper::MESSAGE => ResponseHelper::OK,
            ResponseHelper::INNER_MESSAGE => 'Successfully logged out',
        ], 200);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\Vote;
use App\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ApiVoteController extends Controller
{
    public function store(Request $request)
    {
        $vote = Vote::add(Auth::id(),
            $request->text,
            $request->groups,
            $request->courses,
            $request->roles,
            $request->answers);

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'vote' => $vote]);
    }

    public function index()
    {
        $votes = Vote::getAll(Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'votes' => $votes]);
    }

    public function answer($id, Request $request)
    {
        try{
            $vote = Vote::updateAnswer($id, Auth::id(), $request->answer['id']);
        } catch (\Exception $e)
        {
            return response()->json([
                ResponseHelper::MESSAGE => ResponseHelper::ERROR,
                ResponseHelper::INNER_MESSAGE => $e->getMessage()
                ], 400);
        }

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'vote' => $vote]);
    }

    public function finish($id)
    {
        $vote = Vote::finish($id, Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'vote' => $vote]);
    }

    public function read()
    {
        $votes = Vote::getAllRead(Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'votes' => $votes]);
    }

    public function unread()
    {
        $votes = Vote::getAllUnread(Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'votes' => $votes]);
    }

    public function updated()
    {
        $votes = Vote::getAllUpdated(Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'votes' => $votes]);
    }
}

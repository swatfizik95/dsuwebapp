<?php

namespace App\Http\Controllers\Api;

use App\Models\Course;
use App\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiCourseController extends Controller
{
    public function index()
    {
        $groups = Course::getAll();

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'courses' => $groups]);
    }
}

<?php

namespace App\Http\Controllers\Api;
use App\Models\Group;
use App\Models\Notification;
use App\Models\NotificationDelivery;
use App\Models\User;
use App\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiNotificationController extends Controller
{
    public function store(Request $request)
    {
        $notification = Notification::add(Auth::id(),
            $request->message,
            $request->groups,
            $request->courses,
            $request->roles);

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'notification' => $notification]);
    }

    public function index()
    {
        $notifications = Notification::getAll(Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'notifications' => $notifications]);
    }

    public function read()
    {
        $notifications = Notification::getAllRead(Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'notifications' => $notifications]);
    }

    public function unread()
    {
        $notifications = Notification::getAllUnread(Auth::id());

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'notifications' => $notifications]);
    }

    public function show($id)
    {
        $notification = Notification::get(Auth::id(), $id);

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'notification' => $notification]);
    }
}

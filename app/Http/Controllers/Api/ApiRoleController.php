<?php

namespace App\Http\Controllers\Api;

use App\Models\Role;
use App\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiRoleController extends Controller
{
    public function students()
    {
        $roles = Role::getRoleOfStudents();

        return response()->json([ResponseHelper::MESSAGE => ResponseHelper::OK, 'roles' => $roles]);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: swatfizik
 * Date: 20.02.2019
 * Time: 13:44
 */

namespace App;


class ResponseHelper
{
    const OK = 'ok';
    const ERROR = 'error';

    const MESSAGE = 'message';
    const INNER_MESSAGE = 'inner_message';
}
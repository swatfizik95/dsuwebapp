<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/tested', function (){
    return response()->json(['kavo' => 'chavo'], 418);
});

Route::middleware('auth:api')->get('/test', function (){
    return 'test';
});

Route::group(['namespace' => 'Api'], function (){
    Route::post('/login', 'ApiAuthController@login');

    Route::group(['middleware' => 'auth:api'], function (){
        Route::get('/user', function (Request $request) {
            return $request->user();
        });

        Route::group(['prefix' => 'roles'], function (){
            Route::get('/', function (Request $request) {
                return $request->user()->roles()->select('name')->pluck('name');
            });

            Route::get('/students', 'ApiRoleController@students');
        });

        Route::get('/logout', 'ApiAuthController@logout');

        Route::group(['prefix' => 'notifications'], function (){
            //Route::get('/', function (){return 'notif';});
            Route::post('/create', 'ApiNotificationController@store');
            Route::get('/', 'ApiNotificationController@index');
            Route::get('/read', 'ApiNotificationController@read');
            Route::get('/unread', 'ApiNotificationController@unread');
            Route::get('/{id}', 'ApiNotificationController@show');
        });

        Route::group(['prefix' => 'votes'], function (){
            Route::post('/create', 'ApiVoteController@store');
            Route::get('/', 'ApiVoteController@index');
            Route::post('{id}/answer', 'ApiVoteController@answer');
            Route::post('{id}/finish', 'ApiVoteController@finish');
            Route::get('/read', 'ApiVoteController@read');
            Route::get('/unread', 'ApiVoteController@unread');
            Route::get('/updated', 'ApiVoteController@updated');
        });

        Route::group(['prefix' => 'groups'], function (){
            Route::get('/', 'ApiGroupController@index');
            Route::get('/search/{query}', 'ApiGroupController@search');
        });

        Route::group(['prefix' => 'courses'], function (){
            Route::get('/', 'ApiCourseController@index');
        });
    });
});



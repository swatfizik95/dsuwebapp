<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->integer('created_by');
            $table->integer('vote_status_id');
            $table->integer('answer_id')->nullable();
            $table->integer('status_id');
            $table->timestamps();
        });

        DB::table(\App\Models\Status::TABLE)->insert(array(
            [
                'description' => 'голосование активно',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\Vote::TABLE
            ],
            [
                'description' => 'голосование не активно',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\Vote::TABLE
            ],
            [
                'description' => 'голосование завершено',
                'code' => \App\Models\Status::CODE_FINISHED,
                'table' => \App\Models\Vote::TABLE
            ],
            [
                'description' => 'голосование началось',
                'code' => \App\Models\Status::CODE_STARTED,
                'table' => \App\Models\Vote::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}

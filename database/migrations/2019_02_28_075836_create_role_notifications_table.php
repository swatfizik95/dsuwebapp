<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notification_id');
            $table->integer('role_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'связь роль уведомление активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\RoleNotification::TABLE
            ],
            [
                'description' => 'связь роль уведомление не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\RoleNotification::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_notifications');
    }
}

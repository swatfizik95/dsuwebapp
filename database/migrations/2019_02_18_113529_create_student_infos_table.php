<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Status;

class CreateStudentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('patronymic');
            $table->integer('user_id');
            $table->integer('group_id');
            $table->integer('course_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'информация о студенте активна',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\StudentInfo::TABLE
            ],
            [
                'description' => 'информация о студенте не активна',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\StudentInfo::TABLE
            ],
        ));

        DB::table('student_infos')->insert(array(
            [
                'name' => 'Вугар',
                'surname' => 'Панахов',
                'patronymic' => 'Садяр оглы',
                'user_id' => 2,
                'group_id' => 1,
                'course_id' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\StudentInfo::TABLE),
            ],
            [
                'name' => 'Расул',
                'surname' => 'Гаджиалиханов',
                'patronymic' => 'Мурадович',
                'user_id' => 3,
                'group_id' => 2,
                'course_id' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\StudentInfo::TABLE),
            ],
            [
                'name' => 'Сакит',
                'surname' => 'Алиев',
                'patronymic' => 'Махаммадалиевич',
                'user_id' => 4,
                'group_id' => 1,
                'course_id' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\StudentInfo::TABLE),
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_infos');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Status;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->integer('status_id');
        });

        DB::table(Status::TABLE)->insert(array(
            [
                'description' => 'курс активен',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\Course::TABLE
            ],
            [
                'description' => 'курс не активен',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\Course::TABLE
            ],
        ));

        DB::table(\App\Models\Course::TABLE)->insert(array(
            [
                'number' => 1,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Course::TABLE),
            ],
            [
                'number' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Course::TABLE),
            ],
            [
                'number' => 3,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Course::TABLE),
            ],
            [
                'number' => 4,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Course::TABLE),
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Status;

class CreateNotificationDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('notification_id');
            $table->integer('delivery_status_id');
            $table->integer('status_id');
            $table->timestamps();
        });

        DB::table(Status::TABLE)->insert(array(
            [
                'description' => 'доставка об уведомлении активно',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\NotificationDelivery::TABLE
            ],
            [
                'description' => 'доставка об уведомлении не активно',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\NotificationDelivery::TABLE
            ],
            [
                'description' => 'уведомление прочитано',
                'code' => Status::CODE_READED,
                'table' => \App\Models\NotificationDelivery::TABLE
            ],
            [
                'description' => 'уведомление не прочитано',
                'code' => Status::CODE_UNREADED,
                'table' => \App\Models\NotificationDelivery::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_deliveries');
    }
}

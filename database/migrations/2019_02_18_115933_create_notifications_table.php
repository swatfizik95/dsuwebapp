<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Status;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by');
            $table->text('message');
            $table->integer('user_id')->nullable();
            $table->integer('status_id');
            $table->timestamps();
        });

        DB::table(Status::TABLE)->insert(array(
            [
                'description' => 'уведомление активно',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\Notification::TABLE
            ],
            [
                'description' => 'уведомление не активно',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\Notification::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}

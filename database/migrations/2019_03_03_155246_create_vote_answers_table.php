<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->integer('vote_id');
            $table->integer('status_id');
        });

        DB::table(\App\Models\Status::TABLE)->insert(array(
            [
                'description' => 'ответ голосования активен',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\VoteAnswer::TABLE
            ],
            [
                'description' => 'ответ голосования не активен',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\VoteAnswer::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vote_answers');
    }
}

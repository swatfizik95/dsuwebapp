<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('code');
            $table->text('description');
        });

        DB::table('roles')->insert(array(
            [
                'name' => 'admin',
                'code' => \App\Models\Role::ADMIN,
                'description' => 'администратор'
            ],
            [
                'name' => 'secretariat',
                'code' => \App\Models\Role::SECRETARIAT,
                'description' => 'секретариат'
            ],
            [
                'name' => 'teacher',
                'code' => \App\Models\Role::TEACHER,
                'description' => 'учитель'
            ],
            [
                'name' => 'student',
                'code' => \App\Models\Role::STUDENT,
                'description' => 'студент'
            ],
            [
                'name' => 'bachelor',
                'code' => \App\Models\Role::BACHELOR,
                'description' => 'бакалавр'
            ],
            [
                'name' => 'master',
                'code' => \App\Models\Role::MASTER,
                'description' => 'магистр'
            ],
            [
                'name' => 'headman',
                'code' => \App\Models\Role::HEADMAN,
                'description' => 'староста'
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}

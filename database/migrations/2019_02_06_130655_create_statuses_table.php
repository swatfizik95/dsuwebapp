<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;
use App\Models\Status;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('code');
            $table->string('table');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'пользователь активен',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\User::TABLE
            ],
            [
                'description' => 'пользователь не активен',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\User::TABLE
            ],
        ));

        DB::table('users')->insert(array(
            [
                'login' => 'admin',
                'name' => 'Деканат',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'login' => 'swatfizik',
                'name' => 'Панахов В. С.',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'login' => 'rasul',
                'name' => 'Гаджиалиханов Р. М.',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'login' => 'sakit',
                'name' => 'Алиев С. М.',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'login' => 'teacher',
                'name' => 'Якубов А. З.',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}

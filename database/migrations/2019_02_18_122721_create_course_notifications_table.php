<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->integer('notification_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'связь курс уведомление активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\CourseNotification::TABLE
            ],
            [
                'description' => 'связь курс уведомление не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\CourseNotification::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_notifications');
    }
}

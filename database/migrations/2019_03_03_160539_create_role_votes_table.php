<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vote_id');
            $table->integer('role_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'связь роль голосование активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\RoleVote::TABLE
            ],
            [
                'description' => 'связь роль голосование не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\RoleVote::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_votes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->integer('vote_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'связь курс голосование активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\CourseVote::TABLE
            ],
            [
                'description' => 'связь курс голосование не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\CourseVote::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_votes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Status;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status_id');
        });

        DB::table(Status::TABLE)->insert(array(
            [
                'description' => 'группа активна',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\Group::TABLE
            ],
            [
                'description' => 'группа не активна',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\Group::TABLE
            ],
        ));

        DB::table(\App\Models\Group::TABLE)->insert(array(
            [
                'name' => 'ФИиИТ',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Group::TABLE),
            ],
            [
                'name' => 'ПМиИ',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Group::TABLE),
            ],
            [
                'name' => 'МиКН',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Group::TABLE),
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}

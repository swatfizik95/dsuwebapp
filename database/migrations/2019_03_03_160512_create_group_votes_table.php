<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('vote_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'связь группа голосование активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\GroupVote::TABLE
            ],
            [
                'description' => 'связь группа голосование не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\GroupVote::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_votes');
    }
}

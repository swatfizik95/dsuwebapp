<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('notification_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'связь группа уведомление активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\GroupNotification::TABLE
            ],
            [
                'description' => 'связь группа уведомление не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\GroupNotification::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_notifications');
    }
}

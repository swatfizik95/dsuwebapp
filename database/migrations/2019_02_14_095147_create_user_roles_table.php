<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Status;
use App\Models\Role;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id');
            $table->integer('user_id');
            $table->integer('status_id');
        });

        DB::table('statuses')->insert(array(
            [
                'description' => 'связь пользователь роль активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\UserRole::TABLE
            ],
            [
                'description' => 'связь пользователь роль не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\UserRole::TABLE
            ],
        ));

        DB::table('user_roles')->insert(array(
            [
                'role_id' => Role::getId(Role::ADMIN),
                'user_id' => 1,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::STUDENT),
                'user_id' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::MASTER),
                'user_id' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::HEADMAN),
                'user_id' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::STUDENT),
                'user_id' => 3,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::MASTER),
                'user_id' => 3,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::STUDENT),
                'user_id' => 4,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::MASTER),
                'user_id' => 4,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
            [
                'role_id' => Role::getId(Role::ADMIN),
                'user_id' => 5,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\UserRole::TABLE)
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}

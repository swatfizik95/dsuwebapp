<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote_deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('vote_id');
            $table->integer('delivery_status_id');
            $table->integer('answer_id')->nullable();
            $table->integer('status_id');
            $table->timestamps();
        });

        DB::table(\App\Models\Status::TABLE)->insert(array(
            [
                'description' => 'доставка об голосовании активно',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\VoteDelivery::TABLE
            ],
            [
                'description' => 'доставка об голосовании не активно',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\VoteDelivery::TABLE
            ],
            [
                'description' => 'голосование прочитано',
                'code' => \App\Models\Status::CODE_READED,
                'table' => \App\Models\VoteDelivery::TABLE
            ],
            [
                'description' => 'голосование не прочитано',
                'code' => \App\Models\Status::CODE_UNREADED,
                'table' => \App\Models\VoteDelivery::TABLE
            ],
            [
                'description' => 'голосование обновлено',
                'code' => \App\Models\Status::CODE_UPDATED,
                'table' => \App\Models\VoteDelivery::TABLE
            ],
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vote_deliveries');
    }
}
